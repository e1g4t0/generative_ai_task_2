package com.example.demo.entity;
import lombok.Data;
import javax.persistence.*;
@Entity
@Data
public class Genre {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String description;
}
