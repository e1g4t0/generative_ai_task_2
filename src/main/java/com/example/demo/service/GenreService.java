package com.example.demo.service;

import com.example.demo.entity.Genre;
import com.example.demo.repository.GenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class GenreService {
    @Autowired
    private GenreRepository genreRepository;

    public List<Genre> getAllGenres() {
        return genreRepository.findAll();
    }

    public Genre getGenreById(Long id) {
        return genreRepository.findById(id).orElse(null);
    }

    public Genre createGenre(Genre genre) {
        return genreRepository.save(genre);
    }

    public Genre updateGenre(Long id, Genre updatedGenre) {
        Genre genre = genreRepository.findById(id).orElse(null);
        if (genre != null) {
            genre.setTitle(updatedGenre.getTitle());
            genre.setDescription(updatedGenre.getDescription());
            // Update other fields if necessary
            return genreRepository.save(genre);
        }
        return null;
    }

    public void deleteGenre(Long id) {
        genreRepository.deleteById(id);
    }
}

