package com.example.demo;

import com.example.demo.entity.Author;
import com.example.demo.repository.AuthorRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class AuthorRepositoryTest {
    @Autowired
    private AuthorRepository authorRepository;

    @Test
    public void testSaveAuthor() {
        Author author = new Author();
        author.setFirstName("John");
        author.setLastName("Doe");

        Author savedAuthor = authorRepository.save(author);
        assertNotNull(savedAuthor.getId());
        assertEquals("John", savedAuthor.getFirstName());
    }

    @Test
    public void testFindById() {
        Long authorId = 1L;
        Author author = new Author();
        author.setId(authorId);
        author.setFirstName("Jane");
        author.setLastName("Smith");

        authorRepository.save(author);

        Optional<Author> foundAuthor = authorRepository.findById(authorId);
        assertNotNull(foundAuthor.isPresent());
        assertEquals("Jane", foundAuthor.get().getFirstName());
    }

    // Add tests for update and delete operations
}

