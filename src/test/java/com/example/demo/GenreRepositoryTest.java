package com.example.demo;

import com.example.demo.entity.Genre;
import com.example.demo.repository.GenreRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class GenreRepositoryTest {
    @Autowired
    private GenreRepository genreRepository;

    @Test
    public void testSaveGenre() {
        Genre genre = new Genre();
        genre.setTitle("Fiction");
        genre.setDescription("Fictional books");

        Genre savedGenre = genreRepository.save(genre);
        assertNotNull(savedGenre.getId());
        assertEquals("Fiction", savedGenre.getTitle());
    }

    @Test
    public void testFindById() {
        Long genreId = 1L;
        Genre genre = new Genre();
        genre.setId(genreId);
        genre.setTitle("Mystery");
        genre.setDescription("Mystery books");

        genreRepository.save(genre);

        Optional<Genre> foundGenre = genreRepository.findById(genreId);
        assertNotNull(foundGenre.isPresent());
        assertEquals("Mystery", foundGenre.get().getTitle());
    }

    // Add tests for update and delete operations
}

