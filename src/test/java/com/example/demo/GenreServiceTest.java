package com.example.demo;

import com.example.demo.entity.Genre;
import com.example.demo.repository.GenreRepository;
import com.example.demo.service.GenreService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

public class GenreServiceTest {
    @Mock
    private GenreRepository genreRepository;

    @InjectMocks
    private GenreService genreService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetAllGenres() {
        List<Genre> genres = new ArrayList<>();
        genres.add(new Genre());
        when(genreRepository.findAll()).thenReturn(genres);

        List<Genre> result = genreService.getAllGenres();
        assertNotNull(result);
        assertEquals(1, result.size());
    }

    @Test
    public void testGetGenreById() {
        Long genreId = 1L;
        Genre genre = new Genre();
        genre.setId(genreId);
        when(genreRepository.findById(genreId)).thenReturn(Optional.of(genre));

        Genre result = genreService.getGenreById(genreId);
        assertNotNull(result);
        assertEquals(genreId, result.getId());
    }

    // Add more tests for create, update, and delete operations
}

